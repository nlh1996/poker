package model

import (
	"context"
	"log"
	"poker/db"
	"sort"
	"sync"

	"go.mongodb.org/mongo-driver/bson"
)

// RankCache 单例结构体
type RankCache struct {
	mu    sync.Mutex
	cache map[string][]User // key: channel, value: top 300 users
}

// UpdateUser 更新用户分数并调整排行榜
func (rc *RankCache) UpdateUser(channel string, appid string, user User) {
	rc.mu.Lock()
	defer rc.mu.Unlock()

	users := rc.cache[channel+appid]
	initialRank := -1 // 初始排名

	// 查找用户当前排名
	for i, u := range users {
		if u.Uid == user.Uid {
			initialRank = i
			break
		}
	}

	// 更新用户分数
	for i, u := range users {
		if u.Uid == user.Uid {
			users[i].Score = user.Score
			break
		}
	}
	if len(users) < 300 {
		users = append(users, user)
	}

	// 重新排序
	sort.Slice(users, func(i, j int) bool {
		return users[i].Score > users[j].Score
	})

	// 限制最多300个用户
	if len(users) > 300 {
		users = users[:300]
	}

	// 更新缓存
	rc.cache[channel] = users

	// 检测排名变化
	finalRank := -1
	for i, u := range users {
		if u.Uid == user.Uid {
			finalRank = i
			break
		}
	}

	// 如果排名发生变化，立即同步到数据库
	if initialRank != finalRank {
		go rc.SyncToDatabase(channel, appid)
	}
}

// SyncToDatabase 同步排行榜数据到数据库
func (rc *RankCache) SyncToDatabase(channel string, appid string) {
	rc.mu.Lock()
	users := rc.cache[channel]
	rc.mu.Unlock()

	client := db.GetDB()
	col := channel + "_rank"
	collection := client.Collection(col)

	// 清空当前排行榜
	_, err := collection.DeleteMany(context.TODO(), bson.M{"appid": appid})
	if err != nil {
		log.Printf("Failed to clear rank collection for channel %s: %v", channel, err)
		return
	}

	// 将 []model.User 转换为 []interface{}
	var usersInterface []interface{}
	for _, user := range users {
		usersInterface = append(usersInterface, user)
	}

	// 插入新的排行榜数据
	_, err = collection.InsertMany(context.TODO(), usersInterface)
	if err != nil {
		log.Printf("Failed to sync rank data to database for channel %s: %v", channel, err)
		return
	}
	log.Printf("Rank data for channel %s synced successfully", channel)
}

// GetTopUsers 获取指定 channel 的前300名用户
func (rc *RankCache) GetTopUsers(channel string) []User {
	rc.mu.Lock()
	defer rc.mu.Unlock()
	return rc.cache[channel]
}

// LoadFromDatabase 从数据库加载排行榜数据到内存
func (rc *RankCache) LoadFromDatabase() {

}
