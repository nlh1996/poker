package model

var (
	rankCacheInstance *RankCache
)

func Init() {
	rankCacheInstance = &RankCache{
		cache: make(map[string][]User),
	}
	rankCacheInstance.LoadFromDatabase()
}

// GetRankCache 获取 RankCache 的单例实例
func GetRankCache() *RankCache {
	return rankCacheInstance
}
