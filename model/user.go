package model

import (
	"poker/db"

	"go.mongodb.org/mongo-driver/bson"
)

type User struct {
	Uid        string `json:"uid" bson:"_id" binding:"required"`
	Data       string `json:"data" bson:"data" binding:"required"`
	Auth       string `json:"auth" bson:"-" binding:"required"`
	Score      int64  `json:"score" bson:"score"`
	UpdateDate string `bson:"update_date"`
	AppID      string `bson:"appid"`
}

func (u *User) Save(col string, filter interface{}) error {
	update := bson.M{"$set": bson.M{"data": u.Data, "appid": u.AppID, "update_date": u.UpdateDate}}
	return db.UpdateOne(col, filter, update)
}

func (u *User) FindOne(col string, filter interface{}) error {
	return db.FindOne(col, filter, u)
}
