package main

import (
	"poker/db"
	"poker/model"
	"poker/router"
)

func main() {
	// 数据库连接
	db.Init()
	// 游戏数据初始化
	model.Init()

	// // 初始化统计
	// info.Init()

	// // 开始游戏世界时间计时
	// go date.Init()

	// 开启http和websocket服务
	router.Init()
	// r := model.NewRoom()
	// r.AddAI()
	// r.Ready()
}
