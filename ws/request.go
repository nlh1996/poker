package ws

import (
	"encoding/json"
	"log"

	"github.com/gin-gonic/gin"
)

// Request .
type Request struct {
	conn     *Connection
	Msg      *Message
	ByteData []byte
}

// GetConn .
func (r *Request) GetConn() *Connection {
	return r.conn
}

// GetMsgData .
func (r *Request) GetMsgData() map[string]interface{} {
	return r.Msg.Data
}

// GetMsgID .
func (r *Request) GetMsgID() uint32 {
	return r.Msg.ID
}

// Send .
func (r *Request) Send(msg interface{}) (err error) {
	return r.conn.Send(gin.H{"status": 200, "id": r.Msg.ID, "data": msg})
}

// SendErr .
func (r *Request) SendErr(msg interface{}) (err error) {
	return r.conn.Send(gin.H{"status": 400, "id": r.Msg.ID, "data": msg})
}

// SetMsg .
func (r *Request) SetMsg(data []byte) {
	l := len(data) - 1
	r.ByteData = data[17:l]
	r.Msg = &Message{}
	err := json.Unmarshal(data, r.Msg)
	if err != nil {
		log.Println(err)
	}
}

// Bind .
func (r *Request) Bind(v interface{}) error {
	return json.Unmarshal(r.ByteData, v)
}

// Message .
type Message struct {
	ID uint32 `json:"id"`
	// dataLen uint32
	Data map[string]interface{} `json:"data"`
}

// NewMsg .
func NewMsg(id uint32, data map[string]interface{}) *Message {
	return &Message{
		ID:   id,
		Data: data,
	}
}

// GetMsgLen .
// func (m *Message) GetMsgLen() uint32 {
// 	return m.dataLen
// }
