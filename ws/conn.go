package ws

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"log"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// Connection .
type Connection struct {
	ID        string
	wsConnect *websocket.Conn
	inChan    chan []byte
	outChan   chan []byte
	ctx       context.Context
	cancel    context.CancelFunc
	router    *Router
	connected bool
}

// var (
// 	users *model.UserMap
// )

// func init() {
// 	users = model.GetUserMgr()
// }

// NewConnection .
func NewConnection(wsConn *websocket.Conn, r *Router, account string, uid string) (*Connection, error) {
	conn := &Connection{
		wsConnect: wsConn,
		inChan:    make(chan []byte, 2048),
		outChan:   make(chan []byte, 2048),
		router:    r,
		ID:        uid,
		connected: true,
	}
	conn.ctx, conn.cancel = context.WithCancel(context.Background())
	p := GetConnPool()
	p.Set(conn)
	// user := users.GetByID(conn.ID)
	// if user == nil {
	// 	user = model.InitUser(account, uid)
	// }
	// if len(user.CacheMsg) != 0 {
	// 	conn.SendCacheMsg(user.CacheMsg)
	// }
	return conn, nil
}

// Start .
func (conn *Connection) Start() (data []byte, err error) {
	// 启动读协程
	go conn.readLoop()
	// 启动写协程
	go conn.writeLoop()

	for {
		select {
		case data = <-conn.inChan:
			req := &Request{
				conn: conn,
			}
			req.SetMsg(data)
			// 请求跟路由绑定，并发处理请求
			go conn.router.DoMsgHandle(req)

		case <-conn.ctx.Done():
			return
		}
	}
}

// Close .
func (conn *Connection) Close() {
	// if conn.connected {
	// 	conn.wsConnect.Close()
	// 	conn.cancel()
	// 	conn.connected = false
	// 	if u := users.GetByID(conn.ID); u != nil {
	// 		u.SetState(enum.OFFLINE)
	// 	}
	// 	info.SetPlayerNum(-1)
	// 	GetConnPool().DelByID(conn.ID)
	// 	log.Println("连接", conn.ID, "已经关闭！！！")
	// }
}

// Send .
func (conn *Connection) Send(msg interface{}) (err error) {
	msgBytes, err := json.Marshal(msg)
	if err != nil {
		log.Println(err)
		return err
	}

	fmt.Println("SEND->", string(msgBytes))

	select {
	case conn.outChan <- msgBytes:
	case <-conn.ctx.Done():
		err = errors.New("connection is closed")
	}
	return
}

// SendJSON .
func (conn *Connection) SendJSON(msg []byte) (err error) {
	fmt.Println("SEND->", string(msg))
	select {
	case conn.outChan <- msg:
	case <-conn.ctx.Done():
		err = errors.New("connection is closed")
		return
	}
	return nil
}

// SendCacheMsg .
func (conn *Connection) SendCacheMsg(msg [][]byte) {
	for _, v := range msg {
		conn.SendJSON(v)
	}
}

// 内部实现
func (conn *Connection) readLoop() {
	var (
		data []byte
		err  error
	)
	for {
		if _, data, err = conn.wsConnect.ReadMessage(); err != nil {
			goto ERR
		}
		//阻塞在这里，等待inChan有空闲位置
		select {
		case conn.inChan <- data:
		case <-conn.ctx.Done(): // closeChan 感知 conn断开
			goto ERR
		}
	}

ERR:
	conn.Close()
}

func (conn *Connection) writeLoop() {
	var (
		data []byte
		err  error
	)
	for {
		select {
		case data = <-conn.outChan:
			if err = conn.wsConnect.WriteMessage(websocket.TextMessage, data); err != nil {
				goto ERR
			}
		case <-conn.ctx.Done():
			goto ERR
		}
	}

ERR:
	conn.Close()
}

// H .
func H(id int, data interface{}) []byte {
	res := gin.H{"status": 300, "data": data, "id": id}
	msgBytes, err := json.Marshal(res)
	if err != nil {
		log.Panicln(err)
		return nil
	}
	return msgBytes
}
