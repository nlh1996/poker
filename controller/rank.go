package controller

import (
	"fmt"
	"net/http"
	"poker/model"
	"poker/utils"

	"github.com/gin-gonic/gin"
)

func GetRank(c *gin.Context) {
	channel := c.Param("channel")
	appid := c.Param("appid")
	top300 := model.GetRankCache().GetTopUsers(channel + appid)
	c.JSON(http.StatusOK, top300)
}

func UpdateScore(c *gin.Context) {
	channel := c.Param("channel")
	var user model.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
		return
	}

	auth := utils.GenerateAuth(user.Uid + user.AppID + channel)
	fmt.Println(auth)
	// 检查认证信息（这里只是简单示例，实际中需要更复杂的认证逻辑）
	if user.Auth != auth {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid auth"})
		return
	}

	// 更新内存中的排名
	model.GetRankCache().UpdateUser(channel, user.AppID, user)

	c.JSON(http.StatusOK, gin.H{"message": "User score updated successfully"})
}
