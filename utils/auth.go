package utils

import (
	"crypto/sha256"
	"encoding/hex"
)

// 生成 Auth 字段的哈希值
func GenerateAuth(str string) string {
	hash := sha256.New()
	hash.Write([]byte(str))
	return hex.EncodeToString(hash.Sum(nil))
}
